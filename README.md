# GitLab 12.10 Auto DevOps and Secure CI templates

This is an extraction of CI templates as of GitLab 12.10.

This allows you to use these templates if updating your `.gitlab-ci.yml` to use `rules`
is not possible.
(In GitLab 13.0, [Auto DevOps and Secure CI templates transistions to use `rules`](https://gitlab.com/groups/gitlab-org/-/epics/2300))

## How to use

TBD. Consider

- project on GitLab.com
- project on another self-managed instance